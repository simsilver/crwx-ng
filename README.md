# crwx-ng
crwx-ng - cross-platform open source e-book reader using crengine-ng. It is a fork of the [CoolReader](https://github.com/buggins/coolreader) project.

This repository is created from the official CoolReader repository by removing everything that does not belong to the "cr3wx" using the "git-filter-repo" utility.

## Description
crwx-ng is an e-book reader.
In fact, it is a [wxWidgets](https://www.wxwidgets.org/) frontend for the [crengine-ng](https://gitlab.com/coolreader-ng/crengine-ng) library.

Supported platforms: Windows, Linux. Basically all platforms that are supported by crengine-ng, wxWidgets and cmake.

Supported e-book formats: fb2, fb3 (incomplete), epub (non-DRM), doc, docx, odt, rtf, pdb, mobi (non-DRM), txt, html, chm, tcr.

Main functions:
* Ability to display 2 pages at the same time
* Displaying and Navigating Book Contents
* Word hyphenation using hyphenation dictionaries
* Most complete support for FB2 - styles, tables, footnotes at the bottom of the page
* Extensive font rendering capabilities: use of ligatures, kerning, hinting option selection, floating punctuation, simultaneous use of several fonts, including fallback fonts
* Reading books directly from a ZIP archive
* TXT auto reformat, automatic encoding recognition
* Flexible styling with CSS files
* Background pictures, textures, or solid background

## Visuals
#### Main window (Linux, Windows)

TODO: write this...

## Installation
1. To install the program, make sure that all dependencies are installed: wxWidgets and crengine-ng library.

   It is best to use your Linux distribution's package manager to install wxWidgets. Otherwise, download it from https://www.wxwidgets.org/. When installing using your package manager, remember to install the "-dev" variant of the package, for example for Ubuntu use the following command:
```
$ sudo apt install libwxgtk3.2-dev
```

To install crengine-ng follow the instructions https://gitlab.com/coolreader-ng/crengine-ng#installation

2. Compile the program using cmake, for example (we are already in the top source directory):
```
$ mkdir ../crwx-ng-build
$ cd ../crqt-wx-build
$ cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DCMAKE_BUILD_TYPE=Release ../crwx-ng
$ make -j10 VERBOSE=1
$ make install

```

## Contributing
TODO: write this guide...

## Authors and acknowledgment
The list of contributors can be found in the AUTHORS file.

## License
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
