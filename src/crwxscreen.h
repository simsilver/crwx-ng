/***************************************************************************
 *   crwx-ng                                                               *
 *   Copyright (C) 2008 Vadim Lopatin <coolreader.org@gmail.com>           *
 *   Copyright (C) 2023 Aleksey Chernov <valexlin@gmail.com>               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA 02110-1301, USA.                                                   *
 ***************************************************************************/

#ifndef CR_WXSCREEN_INCLUDED
#define CR_WXSCREEN_INCLUDED

#include <crgui.h>

#include <wx/bitmap.h>

/// wxWidget support: draw to wxImage
class CRWxScreen: public CRGUIScreenBase
{
public:
    CRWxScreen(int width, int height) : CRGUIScreenBase(width, height, true) { }
    wxBitmap getWxBitmap() {
        return _wxbitmap;
    }
protected:
    wxBitmap _wxbitmap;
    virtual void update(const lvRect& rc, bool full);
private:
    void convert1bppData_impl(lUInt8* dstData, int width, int height, int dstPitch);
    void convert2bppData_impl(lUInt8* dstData, int width, int height, int dstPitch);
    void convert3bppData_impl(lUInt8* dstData, int width, int height, int dstPitch);
    void convert4bppData_impl(lUInt8* dstData, int width, int height, int dstPitch);
    void convert8bppData_impl(lUInt8* dstData, int width, int height, int dstPitch);
    void convert32bppData_impl(lUInt8* dstData, int width, int height, int dstPitch);
};

#endif // CR_WXSCREEN_INCLUDED
