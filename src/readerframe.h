/***************************************************************************
 *   crwx-ng                                                               *
 *   Copyright (C) 2007,2008 Vadim Lopatin <coolreader.org@gmail.com>      *
 *   Copyright (C) 2020,2023 Aleksey Chernov <valexlin@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA 02110-1301, USA.                                                   *
 ***************************************************************************/

#ifndef READERFRAME_H
#define READERFRAME_H

#include <wx/frame.h>
#include <wx/listbase.h>

#include <crprops.h>

enum active_mode_t
{
    am_none,
    am_book,
    am_history
};

class wxBoxSizer;

class ReaderScrollBar;
class ReaderView;
class HistList;

/**
 * @short Application Main Window
 */
class ReaderFrame: public wxFrame
{
private:
    bool _isFullscreen;
    active_mode_t _activeMode;
    int _toolbarSize;
public:
    ReaderFrame(const wxString& title, const wxString& fnameToOpen, const wxPoint& p, const wxSize& sz);

    void SetActiveMode(active_mode_t mode);
    void UpdateToolbar();
    void OnOptionsChange(CRPropRef oldprops, CRPropRef newprops, CRPropRef changed);

    void OnQuit(wxCommandEvent& event);
    void OnAbout(wxCommandEvent& event);
    void OnScroll(wxScrollEvent& event);
    void OnKeyDown(wxKeyEvent& event);
    void OnSetFocus(wxFocusEvent& event);
    void OnFileOpen(wxCommandEvent& event);
    void OnFileExport(wxCommandEvent& event);
    void OnCommand(wxCommandEvent& event);
    void OnRotate(wxCommandEvent& event);
    void OnShowOptions(wxCommandEvent& event);
    void OnShowTOC(wxCommandEvent& event);
    void OnShowHistory(wxCommandEvent& event);
    void OnUpdateUI(wxUpdateUIEvent& event);
    void OnClose(wxCloseEvent& event);
    void OnMouseWheel(wxMouseEvent& event);
    void OnSize(wxSizeEvent& event);
    void OnInitDialog(wxInitDialogEvent& event);
    void OnHistItemActivated(wxListEvent& event);

    CRPropRef getProps() {
        return _props;
    }
    void SaveOptions();
    void RestoreOptions();
    void SetMenu(bool visible);
    void SetStatus(bool visible);
    void SetToolbarSize(int size);

    wxBitmap getIcon(const lChar32* name);
protected:
    ReaderScrollBar* _scrollBar;
    ReaderView* _view;
    HistList* _hist;
    wxBoxSizer* _sizer;
    wxString _fnameToOpen;
    CRPropRef _props;
private:
    DECLARE_EVENT_TABLE()
};

enum
{
    Menu_File_Quit = 100,
    Menu_File_About,
    Menu_File_Export,
    Menu_File_Options,
    Menu_View_ZoomIn,
    Menu_View_ZoomOut,
    Menu_View_NextPage,
    Menu_View_PrevPage,
    Menu_View_NextLine,
    Menu_View_PrevLine,
    Menu_View_Text_Format,
    Menu_Link_Back,
    Menu_Link_Forward,
    Menu_Link_Next,
    Menu_Link_Prev,
    Menu_Link_Go,
    Menu_View_Begin,
    Menu_View_End,
    Menu_View_ToggleFullScreen,
    Menu_View_TogglePages,
    Menu_View_TogglePageHeader,
    Menu_View_TOC,
    Menu_View_History,
    Menu_View_Rotate,
};

enum
{
    Window_Id_Scrollbar = 1000,
    Window_Id_View,
    Window_Id_HistList,
    Window_Id_Options,
};

#endif // READERFRAME_H
