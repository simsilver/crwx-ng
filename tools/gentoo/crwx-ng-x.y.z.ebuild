# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
WX_GTK_VER="3.2-gtk3"
inherit cmake wxwidgets

DESCRIPTION="Cross-platform e-book reader"
HOMEPAGE="https://gitlab.com/coolreader-ng/crwx-ng"
SRC_URI="https://gitlab.com/coolreader-ng/${PN}/-/archive/${PV}/${P}.tar.bz2"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

CDEPEND="app-text/crengine-ng
        x11-libs/wxGTK:${WX_GTK_VER}[X]"
BDEPEND="${CDEPEND}"
RDEPEND="${CDEPEND}
        virtual/ttf-fonts"

pkg_setup() {
        setup-wxwidgets
}
